<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 

the_post();

?>


  <?php  
    get_template_part("part","breadcrumb");
  ?>

	<section id="hero">
    </section>
    <div class="container">
      
      <section id="sort">
      <ul class="nav nav-pills">

        <?php if(is_user_logged_in()): ?>
          <?php 
          	$a = attended_to_event( get_the_ID() );
          ?>
          <li class="active">
          	<a href="#" class="attend_btn <?php echo $a?"attended":""; ?>" data-attended="<?php echo $a?"1":"0"; ?>" data-id="<?php echo get_the_ID(); ?>">
          		<i class="fa fa-plus fa-fw"></i>
  	        	<span class="attend_txt">Attend to event</span>
  	        	<span class="unattend_txt">Unattend event</span>
          	</a>
          </li>
        <?php else: ?>
          <li class="active">
            <a href="#" class="attend_btn" data-toggle="modal" data-target="#logginRequired">
              Attend to event
            </a>
          </li>
        <?php endif; ?>
      </ul>
      </section>
    <section id="single">
      <div class="col-lg-4 preview">
      	<?php the_post_thumbnail("single_image", 'alt="Event Picture"') ?>
        <!-- img src="images/event_md_1.jpg" alt="Event Picture"> -->
        <div id="attend">
           <h1>Who is attending</h1>
           <ul class="users">

              <?php
                $user = get_userdata( get_current_user_id() );
              ?>
              <li title="<?php echo $user->data->display_name; ?>" class="current_user <?php echo $a?"attended":""; ?>" ><?php echo get_avatar( get_current_user_id(), 60 ) ?></li>

              <?php  
                global $post;
                $users_c = get_users( array(
                  'connected_type' => 'user_to_event',
                  'connected_items' => $post,
                  'fields' => 'ID'
                ) );

                $users = get_users( array(
                  'connected_type' => 'user_to_event',
                  'connected_items' => $post,
                  'number' => 4
                ) );

                $c = 0;

                foreach($users as $user):

                  if(get_current_user_id() == $user->ID) continue;
                  if(++$c == 3) break;

                  $avatar = get_avatar( $user->user_email, 60 );
              ?>

                <li title="<?php echo $user->display_name; ?>"><?php echo $avatar; ?></li>

              <?php  
                endforeach;
              ?>

           </ul>
           <h2>
            <span class="total_attendants" data-count="<?php echo count($users_c); ?>"><?php echo count($users_c); ?></span> students enrolled
          </h2>
        </div>
      </div>
      <div class="col-lg-4 description">
        <h1 class="title"><?php the_title(); ?></h1>
        <div id="content">
          <?php the_content(); ?>
        </div>
      </div>
      <div class="col-lg-4 details">
        <ul class="detail">
        <h1>Author</h1>
          <li class="user">
            <!-- <img src="images/usr2.jpg" alt=""> -->
            <?php  
            	$organizators = new WP_Query( array(
      					'post_type' => 'organizator',
      					"connected_type" => "organizator_to_event",
      					"connected_direction" => "to",
      					"connected_items" => get_the_ID()
    				) );
				$organizators = $organizators->posts;
            ?>
            <span class="name"><?php echo $organizators[0]->post_title; ?></span>
          </li>

          <li>
            <h1>When</h1>
            <?php  
            	$date = new DateTime( get_field("data_fillimit") );
            ?>
            <p><?php echo $date->format("F d, Y") . " at " . $date->format("h:i a"); ?></p>
          </li>

          <li>
            <h1>Where</h1>
            <p><?php the_field("lokacioni"); ?></p>
          </li>

          <li>
          	<?php
	            $event_categories = wp_get_post_terms( get_the_ID(), "events_category" );
	            $arr_event_categories = array();
	            foreach($event_categories as $event_category){
	            	$arr_event_categories[] = "<a href='".get_term_link($event_category)."'>". $event_category->name ."</a>";
	            }
          	?>
            <h1>Cateogry</h1>
            <p><?php echo implode(",",$arr_event_categories); ?></p>
          </li>

        </ul>
      </div>
    </section>  

    <?php

      $event_category_ids = array();
      foreach($event_categories as $event_category){
          $event_category_ids[] = $event_category->term_id;
      }

      $related_events = new WP_Query(array(
        'post_type' => 'event',
        'tax_query' => array(
          'relation' => 'OR',
          array(
            'taxonomy' => 'events_category',
            'field' => 'id',
            'terms' => $event_category_ids
          )
        ),
        "post__not_in" => array(get_the_ID()),
        "posts_per_page" => 3
      ));

      
      if($related_events->have_posts()):
    ?>
    <section class="thumbnails related_events">
      <h3>Related Events</h3>
      <?php  
        set_events_col(4);
        while($related_events->have_posts()): $related_events->the_post();
          get_template_part("event","item"); 
        endwhile;
        reset_events_col();
      ?>
    </section>
    <?php  
      endif;
    ?>
    </div> <!-- /container -->

<?php
get_footer();

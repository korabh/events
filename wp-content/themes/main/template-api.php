<?php 
	/* Template Name: API */

	$key = "8A65ECBBC3EC19C382DA492B4562B";

	global $items_per_page, $page_num, $format;

	$items_per_page = 10;
	if( isset($_REQUEST["items_per_page"]) ){
		$items_per_page = $_REQUEST["items_per_page"];
	}

	$page_num = 1;
	if( isset($_REQUEST["page_num"]) ){
		$page_num = $_REQUEST["page_num"];
	}

	$format = "json";
	if(isset($_REQUEST["format"]) && $_REQUEST["format"]=="xml"){
		$format = "xml";
	}
	if( !isset($_REQUEST["key"]) ){
		$r = array("error"=>true, "message"=>"Key required to access API");

	}else if($_REQUEST["key"] != "8A65ECBBC3EC19C382DA492B4562B"){
		$r = array("error"=>true, "message"=>"Invalid Key");

	}else if(isset($_REQUEST["action"])){

		switch( $_REQUEST["action"] ){

			/* Events */
			case "events":
				// action=events
				$r = getEventsAll();
				break;
			case "events_by_date":
				// action=events_by_date
				// start_date=()
				if(isset($_REQUEST["start_date"]) || isset($_REQUEST["end_date"]) ){
					$data = array();
					if( isset($_REQUEST["start_date"]) && ($validDate=isValidDate($_REQUEST["start_date"], "start date")) !== true ){
						$r = array("error"=>true, "message"=>$validDate);
					}else if( isset($_REQUEST["end_date"]) && ($validDate=isValidDate($_REQUEST["end_date"], "end date")) !== true ){
						$r = array("error"=>true, "message"=>$validDate);
					}else{
						if( isset($_REQUEST["start_date"]) ) $data["start_date"] = getTimestamp( $_REQUEST["start_date"] );
						if( isset($_REQUEST["end_date"]) ) 	 $data["end_date"]   = getTimestamp( $_REQUEST["end_date"] );
						$r = getEventsByDate( $data );
					}
				}else{
					$r = array("error"=>true, "message"=>"Start date OR End date is required");
				}
				break;
			case "events_by_organizator":
				// action=events_by_organizator
				// id=(organizator id)
				if(isset($_REQUEST["id"])){
					$r = getEventsByOrganizator( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Organizator ID is required");
				}
				break;
			case "events_by_status":
				// action=events_by_status
				// id=(status id)
				if(isset($_REQUEST["id"])){
					$r = getEventsByStatus( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Status ID is required");
				}
				break;
			case "events_by_language":
				// action=events_by_language
				// id=(language id)
				if(isset($_REQUEST["id"])){
					$r = getEventsByLanguage( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Language ID is required");
				}
				break;
			case "events_by_category":
				// action=events_by_category
				// id=(category id)
				if(isset($_REQUEST["id"])){
					$r = getEventsByCategory( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Category ID is required");
				}
				break;
			case "search":
				
				$r = searchEventsApi( $_REQUEST );
				
				break;
			case "event":
				// action=event
				// id=(event id)
				if(isset($_REQUEST["id"])){
					$r = getEvent( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Event ID is required");
				}
				break;
			
			/* Languages */
			case "languages":
				// action=languages
				$r = getLanguages();
				break;
			
			/* Categories */
			case "categories":
				// action=categories
				$r = getCategories();
				break;
			
			/* Organizators */
			case "organizatoret":
				// action=organizatoret
				$r = getOrganizators();
				break;
			case "organizator":
				// action=organizator
				// id=(organizator id)
				if(isset($_REQUEST["id"])){
					$r = getOrganizator( $_REQUEST["id"] );
				}else{
					$r = array("error"=>true, "message"=>"Organizator ID is required");
				}
				break;
			
			/* Statuses */
			case "statuses":
				// action=statuses
				$r = getStatuses();
				break;

			default:
				$r = array("error"=>true, "message"=>"Invalid Action"); 
				break;
		}
	}else{
		$r = array("error"=>true, "message"=>"Action is required");
	}

	if($format == "json"){
		echo json_encode($r);
	}else{
		header('Content-type: text/xml');
		echo xml_encode($r);
	}

	/* 
		Get Events
	*/
	function getEventsAll(){
		$params = array("post_type"=>"event");
		$params = addPaginationParams( $params );
		return getEvents( new WP_Query($params) );
	}

	/*
		Get Events By Start Date, End Date, Or Both
	*/
	function getEventsByDate( $data ){
		$params = array("post_type"=>"event");
		$params = addPaginationParams( $params );
		$meta_query = array("relation"=>"AND");
		if(isset($data["start_date"])){
			$meta_query[] = array(
				'key' => 'data_fillimit',
				'value' => $data["start_date"],
				'compare' => '>=',
				'type' => "NUMERIC"
			);
		}
		if(isset($data["end_date"])){
			$meta_query[] = array(
				'key' => 'data_mbarrimit',
				'value' => $data["end_date"],
				'compare' => '<=',
				'type' => "NUMERIC"
			);
		}
		$params["meta_query"] = $meta_query;
		return getEvents( new WP_Query($params) );
	}

	/*
		Get Events by organizators ID
	*/
	function getEventsByOrganizator( $id ){
		$params = array(
			"post_type"=>"event",
			"connected_type" => "organizator_to_event",
			"connected_direction" => "from",
			"connected_items" => $id
		);
		$params = addPaginationParams( $params );
		return getEvents( new WP_Query($params) );
	}

	/*
		Get Events by status
	*/
	function getEventsByStatus( $id ){
		$term = get_term_by("term_id",$id,"status");
		$params = array(
			"post_type"=>"event",
			"status" => $term->slug
		);
		$params = addPaginationParams( $params );
		return getEvents( new WP_Query($params) );
	}

	/*
		Get Events by language
	*/
	function getEventsByLanguage( $id ){
		$term = get_term_by("term_id",$id,"language");
		$params = array(
			"post_type"=>"event",
			"language" => $term->slug
		);
		$params = addPaginationParams( $params );
		return getEvents( new WP_Query($params) );
	}

	/*
		Get Events by category
	*/
	function getEventsByCategory( $id ){
		$term = get_term_by("term_id",$id,"events_category");
		$params = array(
			"post_type"=>"event",
			"events_category" => $term->slug
		);
		$params = addPaginationParams( $params );
		return getEvents( new WP_Query($params) );
	}

	
	/*
	*/
	function searchEventsApi( $data ){
		$r = searchEvents( $data );
		
		if(is_array($r) && isset($r["error"])) return $r;

		return getEvents( $r );
	}


	/* 
		Get Events by providing a WP_Query object
	*/
	function getEvents( $query ){
		$posts = array();
		foreach( $query->posts as $post ){
			$posts[] = getEvent( $post->ID );
		}
		return addPostsInfo( $posts, $query );	
	}

	/* 
		Get Event
	*/
	function getEvent( $id ){
		$post = get_post( $id );

		if($post->post_type != "event") return array("error"=>true, "message"=>"Invalid Event ID");

		$coord = explode(",", get_field("koordinatat", $id ) );

		$post_obj = array(
			"id" => $post->ID,
			"titulli" => $post->post_title,
			"komenti" => $post->post_content,
			"data_fillimit" => get_field("data_fillimit", $id ),
			"data_mbarrimit" => get_field("data_mbarrimit", $id ),
			"lokacioni" => get_field("lokacioni", $id ),
			"periodike" => get_field("periodike", $id ),
			"koordinatat" => isset($coord[1]) ?array("lat"=>$coord[0], "lng"=>$coord[1]) :"undefined",
			"gjinia_dedikuar" => get_field("gjinia_dedikuar", $id ),
			"gjuha" => getLanguages($id),
			"organizatoret" => getOranizatorsByEventId( $id ),
			"status" => getStatuses( $id ),
			"kategoria" => getCategories( $id )
		);
		return $post_obj;
	}

	/* 
		Get Languages
		$event_id - get langauges of specific event
	*/
	function getLanguages($event_id=null){
		if($event_id != null){
			$langs = wp_get_post_terms($event_id, 'language');
		}else{
			$langs = get_terms("language", array("hide_empty"=>false));
		}
		$lang_data = array();
		foreach($langs as $lang){
			$lang_data[] = array(
				"id" => $lang->term_id, 
				"emri" => $lang->name
			);
		}
		return $lang_data;
	}

	/* 
		Get Categories
		$event_id - get categories of specific event
	*/
	function getCategories($event_id=null){
		if($event_id != null){
			$langs = wp_get_post_terms($event_id, 'events_category');
		}else{
			$langs = get_terms("events_category", array("hide_empty"=>false));
		}
		$cat_data = array();
		foreach($langs as $cat){
			$cat_data[] = array(
				"id" => $cat->term_id, 
				"emri" => $cat->name
			);
		}
		return $cat_data;
	}

	/* 
		Get Statuses
		$event_id - get status of specific event
	*/
	function getStatuses($event_id=null){
		if($event_id != null){
			$langs = wp_get_post_terms($event_id, 'status');
		}else{
			$langs = get_terms("status", array("hide_empty"=>false));
		}
		$statuses_data = array();
		foreach($langs as $lang){
			$statuses_data[] = array(
				"id" => $lang->term_id, 
				"emri" => $lang->name
			);
		}
		return $statuses_data;
	}

	/* 
		Get Organizators of given event
	*/
	function getOranizatorsByEventId( $id ){
		$query = new WP_Query(array(
			"post_type" => "organizator",
			"connected_type" => "organizator_to_event",
			"connected_direction" => "to",
			"connected_items" => $id
		));
		return getOrganizatorsByQuery( $query );
	}

	/* 
		Get Organizators by providing a WP_Query object
	*/
	function getOrganizatorsByQuery( $query ){
		$posts = array();
		while($query->have_posts()): $query->the_post();
			$posts[] = array(
				"id" => get_the_ID(),
				"emri" => get_the_title()
			);
		endwhile;
		return $posts;
	}

	/* 
		Get All Organizators
	*/
	function getOrganizators(){
		$params = array( "post_type" => "organizator" );
		$params = addPaginationParams( $params );
		$query = new WP_Query( $params );
		$posts = getOrganizatorsByQuery( $query );
		return addPostsInfo( $posts, $query );
	}

	/* 
		Get Organizator by ID 
	*/
	function getOrganizator( $id ){
		$query = new WP_Query(array(
			"post_type" => "organizator",
			"post__in" => array($id)
		));
		return getOrganizatorsByQuery( $query );
	}

	/* 
		Add Pagination Params to given array
	*/
	function addPaginationParams( $params ){
		global $items_per_page, $page_num;

		$params["posts_per_page"] = $items_per_page;
		$params["paged"] = $page_num;
		return $params;
	}

	/*
		Add Posts info like total items and pages
	*/
	function addPostsInfo( $posts, $query ){
		return array(
			"items_total" => $query->found_posts,
			"pages_total" => $query->max_num_pages,
			"posts" => $posts
		);
	}


	/* HELPER FUNCTIONS */
	function xml_encode($data){
		return array_to_xml($data, new SimpleXMLElement('<root/>'))->asXML();
	}

	function array_to_xml(array $arr, SimpleXMLElement $xml) {
        foreach ($arr as $k => $v) {

            $attrArr = array();
            $kArray = explode(' ',$k);
            $tag = array_shift($kArray);

            if (count($kArray) > 0) {
                foreach($kArray as $attrValue) {
                    $attrArr[] = explode('=',$attrValue);                   
                }
            }

            if (is_array($v)) {
                if (is_numeric($k)) {
                    array_to_xml($v,$xml->addChild("item"));
                } else {
                    $child = $xml->addChild($tag);
                    if (isset($attrArr)) {
                        foreach($attrArr as $attrArrV) {
                            $child->addAttribute($attrArrV[0],$attrArrV[1]);
                        }
                    }                   
                    array_to_xml($v, $child);
                }
            } else {
                $child = $xml->addChild($tag, $v);
                if (isset($attrArr)) {
                    foreach($attrArr as $attrArrV) {
                        $child->addAttribute($attrArrV[0],$attrArrV[1]);
                    }
                }
            }               
        }

        return $xml;
    }


var url = location.protocol + '//' + location.host + location.pathname;

var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    	// If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    	// If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    	// If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();


for(var key in QueryString){
	QueryString[key] = decodeURIComponent( QueryString[key] );
}

function EncodeQueryData(data){
   var ret = [];
   for (var d in data)
      ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
   return ret.join("&");
}

jQuery(document).ready(function($) {

	$(".datepicker").pickadate();

	$(".attend_btn").data("ajax",false).on("click",function(e){
		e.preventDefault();
		var _this = $(this);

		if( $(this).data("ajax") ) return;

		_this.data("ajax",true);
		var attend = _this.data("attended")=="0";
		_this.stop().animate({opacity:0.5},200);
		var count = $(".total_attendants");

		$.ajax({
			type: "POST",
			url: adminAjax,
			data: { 
				action: attend?"attend_event":"unattend_event", 
				event_id: _this.data("id")
			}
		}).done(function( msg ) {

			$(".users .current_user")[attend ?"addClass" :"removeClass"]("attended");
			_this[attend ?"addClass" :"removeClass"]("attended");
			_this.data("attended", attend ?"1" :"0" );
			_this.data("ajax",false);
			_this.stop().animate({opacity:1},200);
			if(attend)
				count.data("count", count.data("count")+1 )
			else
				count.data("count", count.data("count")-1 )

			count.text(count.data("count"));

		});

	});

	// Filters
	function initFilters(){
		var filtersCont = $("#filters");
		var keysSelect = filtersCont.find("#filter_key");
		var activeId = keysSelect.val();
		var filersFields = filtersCont.find(".filter_field");
		filersFields.not("#"+activeId).hide();

		keysSelect.on("change",function(){
			filersFields.filter("#"+activeId).hide();
			activeId = keysSelect.val();
			filersFields.filter("#"+activeId).show();
		});

		$("#addFilter form").on("submit",function(e){
			e.preventDefault();
			if(QueryString['filter'] == undefined){
				QueryString = [];
			}
			QueryString[ activeId ] = filersFields.filter("#"+activeId).val();
			QueryString[ 'filter' ] = 1;
			location.href = homeUrl+"?"+EncodeQueryData(QueryString);
		});

		$(".filter_remove").on("click", function(e){
			e.preventDefault();
			delete QueryString[$(this).data("key")];
			var count = 0;
			for (var k in QueryString) {
			    if (QueryString.hasOwnProperty(k)) {
			       ++count;
			    }
			}
			if(count == 1){
				location.href = homeUrl;
			}else{
				location.href = homeUrl+"?"+EncodeQueryData(QueryString);
			}
		})
	}
	initFilters();

});
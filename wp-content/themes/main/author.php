<?php get_header(); ?>

    <?php  
      $qo = get_queried_object();
    ?>

    <?php  
      get_template_part("part","breadcrumb");
    ?>

    <div class="container">
      <section id="category_title">
      </section>
    

      <section class="thumbnails col-lg-12 np-l" role="main">
        
        <h2>
        <?php echo $qo->user_nicename; ?>
        </h2>

        <?php  
          $data = array(
            "attendant_id" => $qo->data->ID
          );
          $events = searchEvents($data);
        ?>

        <?php
        set_events_col(4);
        if($events->have_posts()):

          while($events->have_posts()): $events->the_post(); 
            get_template_part("event","item"); 
          endwhile; 

        else: 
          ?>

          <div class="col-lg-12">
            No Events found that match the search
          </div>

          <?php 
        endif; 
        reset_events_col();
        ?>


        <?php
        global $wp_query;
        $wp_query = $events;
        if(function_exists("wp_pagenavi"))
          wp_pagenavi();
        ?>

      </section><!-- End of thumbanil -->

<?php
get_footer(); 
?>
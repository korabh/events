
<?php 	
	global $_fields, $events_data;
?>

<section id="sort">
  <ul class="nav nav-pills">
    <li><btn href="#" class="btn btn-default" data-toggle="modal" data-target="#addFilter"><i class="fa fa-plus fa-fw"></i>Add filter</btn></li>
  </ul>
</section>


<!-- Active Filters -->
<?php if( isset( $_GET["filter"] ) ): ?>

<ul class="filters">
  <li class="title">Filters</li>
	<?php
	  $params = $_GET;
	  $events_data = array();
	  foreach($params as $key=>$value):
	    if(in_array($key, $_fields)):
	      $events_data[$key] = $value;
        $filter_data = get_filter_data( $key, $value );
	    ?>
	      <li>
          <span class="filter_label"><?php echo $filter_data["label"]; ?></span>: 
          <span class="filter_value"><?php echo $filter_data["value"]; ?></span> 
          <a data-key="<?php echo $key ?>" class="filter_remove"><span class="icon-remove"></span></a></li>
	    <?php  
	    endif;
	  endforeach;
	?>
</ul>

<?php endif; ?>


<!-- Categories -->

<?php 
  $qo = get_queried_object();
  $active_cat = "";
  if(isset($qo->taxonomy) && $qo->taxonomy == "events_category"){
    $active_cat = $qo->term_id;
  }
	$categories = get_terms("events_category", array(
	  "hide_empty" => false
	));
?>
<ul class="category">
<li class="title">Popular Categories</li>
<?php foreach($categories as $category): ?>
  <li class="<?php echo ($active_cat == $category->term_id) ?'active' :''; ?>"><a href="<?php echo get_term_link($category, "events_category"); ?>"><?php echo $category->name; ?></a></li>
<?php endforeach; ?>
</ul>



<!-- By Date Filter -->

<?php  
  $bd = "";
  if( isset($_REQUEST["bydate"]) ) $bd = $_REQUEST["bydate"];
?>
<ul class="date">
  <li class="title">Dates</li>
  <li class="<?php echo $bd=="" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=all">All dates</a></li>
  <li class="<?php echo $bd=="today" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=today">Today</a> </li>
  <li class="<?php echo $bd=="tomorrow" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=tomorrow">Tomorrow</a> </li>
  <li class="<?php echo $bd=="this_week" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=this_week">This Week</a></li>
  <li class="<?php echo $bd=="this_weekend" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=this_weekend">This Weekend</a></li>
  <li class="<?php echo $bd=="next_week" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=next_week">Next Week</a></li>
  <li class="<?php echo $bd=="next_month" ?"active" :""; ?>"><a href="<?php echo HOME; ?>?bydate=next_month">Next Month</a></li>
</ul>
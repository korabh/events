<section id="slider">
      <div class="container">
      <div class="col-lg-12 np-l">
        <div class="col-lg-8 np-l">
          <div class="discovery">
            <h1>Find your perfect event</h1>
            <h3>Brings people together through live<br>experiences discover events that match<br> your passions.</h3> 
          </div>
        </div>
        <div class="col-lg-4">
          <div class="search">
            <form role="form" action="#">
            <div class="form-group">
              <input name="search_term" type="text" class="form-control" id="search_term" placeholder="Search Term">
            </div>
            <div class="form-group">
              <select class="form-control input-lg" name="city_id">
                <option value="">Qyteti</option>
                <?php $cities = get_all_cities(); ?>
                <?php foreach($cities as $city): ?>
                  <option value="<?php echo $city->term_id; ?>"><?php echo $city->name; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <div class="col-lg-6 np-l np-r">
                <input type="text" class="form-control datepicker" id="date" placeholder="Date" name="date">
              </div>
            <div class="col-lg-6 np-r">
              <button type="submit" class="btn btn-search btn-lg btn-block">Search</button>
            </div>
            <div class="form-group count">
              <?php  
                $events_count = wp_count_posts("event");
              ?>
              <h5>We are counting  <b><?php echo $events_count->publish; ?> events</b> </h5>
            </div>
            </div>
          </form>
        </div>
          </div>
        </div>
      </div>
</section>
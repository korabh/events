<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
  <script type="text/javascript">
    var adminAjax = "<?php echo admin_url('admin-ajax.php'); ?>";
    var homeUrl = "<?php echo HOME; ?>";
  </script>
  
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <div class="modal fade" id="logginRequired" tabindex="-1" role="dialog" aria-labelledby="logginRequiredLabel" aria-hidden="true">
    <form action="<?php echo HOME; ?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="alert alert-info">You should be logged in to attend to event</div>
          <div class="modal-body">
            <?php echo do_shortcode("[wppb-login]"); ?>
            <?php do_action( 'social_connect_form' ); ?>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" value="Add">Close</button>
          </div>
        </div>
      </div>
    </form>
  </div>


  <?php get_template_part("modal_add_filter"); ?>


  <!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?php bloginfo('home'); ?>"><img src="<?php echo TEMPLATE_URL."/images/logo.png"; ?>" alt=""></a>        
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar pull-right">
<!--        <li><a href="#">Home</a></li>
          <li><a href="#about">About</a></li>
          <li><a href="#contact">Contact</a></li> -->
          <li class="author">
            <?php          
              if(is_user_logged_in()){
                $userdata = get_userdata(get_current_user_id());
                $userdata = $userdata->data;
              ?> 
                <span class="logged_user">Logged in as <a href="<?php echo get_author_posts_url(get_current_user_id()); ?>"><?php echo $userdata->display_name ?></a></span>
                <a href="<?php echo wp_logout_url(home_url()); ?>">Logout</a> 

                <a href="<?php echo get_author_posts_url( get_current_user_id() ); ?>">
                  <?php echo get_avatar( get_current_user_id(), 30 ) ?>
                  <!-- <span id="name">Korab Hoxha</span> -->
                </a>
              <?php
              }else{
              ?> 
                <a href="<?php the_field("login_page","options"); ?>">Login</a> 
                <a href="<?php the_field("register_page","options"); ?>">Register</a> 
              <?php
              }
            ?>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>

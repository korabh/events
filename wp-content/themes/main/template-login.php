<?php  
	/* Template Name: Login */
?>

<?php  
	if(is_user_logged_in()){
		wp_redirect(home_url());
		exit;
	}
?>

<?php get_header(); ?>


    <div class="container">


    	<h2>Login</h2>
    	<?php echo do_shortcode("[wppb-login]"); ?>
    	<?php do_action( 'social_connect_form' ); ?>


<?php
get_footer(); 
?>


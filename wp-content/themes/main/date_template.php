<?php get_header(); ?>

    <?php  
      get_template_part("part","breadcrumb");
    ?>

    <div class="container">
    
      <section class="col-lg-3 sidebar">
          <?php get_sidebar(); ?>
      </section><!-- End of sidebar -->

      <section class="thumbnails col-lg-9 np-l" role="main">
        <?php 

          $today = strtotime("today");
          $oneday = strtotime("today+1day");
          $twodays = strtotime("today+2days");
          $thisweekstart = strtotime("last monday");
          $thisweekendstart = strtotime("last monday+5 days");
          $thisweekend = strtotime("last monday+1week");
          $nextweekend = strtotime("last monday+2weeks");
          $nextmonthstart = strtotime("first day of this month + 1 month", $today );
          $nextmonthend = strtotime("first day of this month + 2 months", $today );

          $date_data = null;
          switch($_REQUEST[ "bydate"]){
            case "today": $date_data=array( "start_date" => $today, "end_date" => $oneday );  break;
            case "tomorrow": $date_data=array( "start_date" => $oneday, "end_date" => $twodays );  break;
            case "this_week": $date_data=array( "start_date" => $thisweekstart, "end_date" => $thisweekend );  break;
            case "this_weekend": $date_data=array( "start_date" => $thisweekendstart, "end_date" => $thisweekend );  break;
            case "next_week": $date_data=array( "start_date" => $thisweekend, "end_date" => $nextweekend );  break;
            case "next_month": $date_data=array( "start_date" => $nextmonthstart, "end_date" => $nextmonthend );  break;
          }

          $events = searchEvents($date_data);
          
          if( is_array($events) && isset( $events["error"] ) ):

            // If there is a error, show it
            ?>

              <p class="bg-danger"><?php echo $events["message"]; ?></p>

            <?php 
          else:

              if($events->have_posts()):

                while($events->have_posts()): $events->the_post(); 
                  get_template_part("event","item"); 
                endwhile; 

              else: 
                ?>

                <div class="col-lg-12">
                  <div class="no_results">No Events found that match the search</div>
                </div>

                <?php 
              endif; 

          endif;
          ?>


        <?php
        global $wp_query;
        $wp_query = $events;
        if(function_exists("wp_pagenavi"))
          wp_pagenavi();
        ?>
        
      </section><!-- End of thumbanil -->

<?php
  get_footer(); 
?>
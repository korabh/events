<?php

date_default_timezone_set("Europe/Belgrade");

define("TEMPLATE_URL", get_template_directory_uri() );
define("HOME", home_url() );
add_filter('show_admin_bar', '__return_false');


global $_fields, $events_data;
$_fields = array("search_term","s","city_id","category_id","language_id", "status_id" ,"organizator_id", "periodic", "gender","start_date", "end_date" ); 



function my_custom_post_status(){
	register_post_status( 'past', array(
		'label'                     => _x( 'Past', 'event' ),
		'public'                    => false,
		'exclude_from_search'       => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Past <span class="count">(%s)</span>', 'Past <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'my_custom_post_status' );



function load_custom_template() {
	if ( isset($_REQUEST["bydate"]) ) {
		include (TEMPLATEPATH . '/date_template.php');
		exit;	
	}elseif ( isset($_REQUEST["filter"]) ) {
		include (TEMPLATEPATH . '/filter_template.php');
		exit;	
	}elseif ( isset($_REQUEST["search_term"]) ) {
		include (TEMPLATEPATH . '/search.php');
		exit;	
	}elseif ( isset($_REQUEST["check_events"]) ) {
		check_recurring_events();	
		exit;	
	}elseif ( isset($_REQUEST["parent_events"]) ) {
		parent_events();	
		exit;	
	}elseif ( isset($_REQUEST["notify_users"]) ) {
		notify_users();	
		exit;	
	}
}
 
add_action('template_redirect', 'load_custom_template');


function custom_author_archive( &$query ) {
    if ($query->is_author)
        $query->set( 'post_type', array( 'event' ) );
}
add_action( 'pre_get_posts', 'custom_author_archive' );


function my_connection_types() {
	p2p_register_connection_type( array(
		'name' => 'organizator_to_event',
		'from' => 'organizator',
		'to' => 'event',
		'reciprocal' => false
	) );


	p2p_register_connection_type( array(
		'name' => 'user_to_event',
		'from' => 'user',
		'to' => 'event',
		'duplicate_connections' => false,
		'fields' => array(
			'notified_24' => array(
	            'title' => 'Notified before 24 hours',
	            'type' => 'text',
	            'default' => "0"
        	),
        	'notified_3' => array(
	            'title' => 'Notified before 3 hours',
	            'type' => 'text',
	            'default' => "0"
        	),
        	'notify_method_24' => array(
	            'title' => 'Notify method before 24 hours',
	            'type' => 'text',
	            'default' => "0"
        	),
        	'notify_method_3' => array(
	            'title' => 'Notify method before 3 hours',
	            'type' => 'text',
	            'default' => "0"
        	)
		)
	) );
}
add_action( 'p2p_init', 'my_connection_types' );



if ( ! function_exists( 'thebase_setup' ) ) :
function thebase_setup() {

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 370, 198, true );
	add_image_size("single_image", 327, 230, true );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array( 
		'primary'   => __( 'Top primary menu', 'thebase' )
	) );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // thebase_setup
add_action( 'after_setup_theme', 'thebase_setup' );


/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */

function thebase_scripts() {

	// Dequeue Facebook Login Plugins Bootstrap
	wp_dequeue_style("awd_fcbk-ui-bootstrap");
	
	// Stylesheets
	wp_enqueue_style('default', get_template_directory_uri() . '/css/default.css' );
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style('picker', get_template_directory_uri() . '/js/datepicker/themes/default.css' );
	wp_enqueue_style('datepicker', get_template_directory_uri() . '/js/datepicker/themes/default.date.css' );
	
	// Javascript
	wp_enqueue_script( 'jquery' );
	// wp_enqueue_script( 'thebase-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.8');
	wp_enqueue_script('picker', get_template_directory_uri() . '/js/datepicker/picker.js' );
	wp_enqueue_script('datepicker', get_template_directory_uri() . '/js/datepicker/picker.date.js', array("picker") );
	wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js' , array("datepicker") );

}
add_action( 'wp_enqueue_scripts', 'thebase_scripts' );


/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function thebase_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} else {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}

	return $classes;
}
add_filter( 'body_class', 'thebase_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function thebase_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'thebase_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function thebase_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'thebase' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'thebase_wp_title', 10, 2 );



function attend_event( $event_id ){
	if(is_user_logged_in()){
		if( !attended_to_event($event_id) ){
			$user_id = get_current_user_id();
			p2p_create_connection( 'user_to_event', array(
				'from' => $user_id,
				'to' => $event_id,
				'meta' => array("notified_3"=>"0", "notified_24"=>"0","notify_method_3"=>"0","notify_method_24"=>"0")
			) );
		}
	}
}

function unattend_event( $event_id ){
	if(is_user_logged_in()){
		if( attended_to_event($event_id) ){
			$user_id = get_current_user_id();
			p2p_delete_connections( 'user_to_event', array(
				'from' => $user_id,
				'to' => $event_id
			) );
		}
	}
}

function attended_to_event( $event_id ){
	$user_id = get_current_user_id();
	if(is_user_logged_in()){
		global $user_events;
		if($user_events == ""){
			$user_events = array();
			$user_events_q = new WP_Query(array(
				"post_type" => "event",
				"connected_type" => "user_to_event",
				"connected_direction" => "from",
				"connected_items" => $user_id,
				"posts_per_page" => -1
			));
			foreach($user_events_q->posts as $post){
				$user_events[] = $post->ID;
			}
		}
		return in_array($event_id, $user_events);
	}
	return false;
}



add_action( 'wp_ajax_attend_event', 'ajax_attend_event' );
add_action( 'wp_ajax_unattend_event', 'ajax_unattend_event' );
function ajax_attend_event(){
	attend_event( $_POST["event_id"] );
}
function ajax_unattend_event(){
	unattend_event( $_POST["event_id"] );
}



function remove_search_filter( $url ){

}

function add_search_filter(){

}



/*
	Search Events
		kategoria - category_id *
		gjuha - language_id *
		status - status_id *
		organizatoret - organizator_id *
		periodike - periodic *
		gjinia_dedikuar - gender *
		data_fillimit - start_date *
		data_mbarrimit - end_date *
*/
function searchEvents( $data, $return_wp_query=false ){

	$params = array( "post_type"=>"event" );
	$params["meta_query"] = array(
		"relation" => "AND"
	);

	
	if( isset( $data["search_term"] ) && trim($data["search_term"]) != "" ){
		$params["s"] = $data["search_term"];
	}elseif( isset( $data["s"] ) && trim($data["s"]) != "" ){
		$params["s"] = $data["s"];
	}

	// Category
	if( isset( $data["category_id"] ) && trim($data["category_id"]) != "" ){
		$category_term = get_term_by("term_id" ,$data["category_id"] ,"events_category");

		if($category_term == "") return array("error"=>true, "message"=>"Invalid Category ID");

		$params["events_category"] = $category_term->slug;
	}

	// City
	if( isset( $data["city_id"] ) && trim($data["city_id"]) != "" ){
		$city_term = get_term_by("term_id" ,$data["city_id"] ,"qyteti");

		if($city_term == "") return array("error"=>true, "message"=>"Invalid City ID");

		$params["qyteti"] = $city_term->slug;
	}

	// Language 
	if( isset( $data["language_id"] ) && trim($data["language_id"]) != "" ){
	$language_term = get_term_by("term_id" ,$data["language_id"] ,"language");

	if($language_term == "") return array("error"=>true, "message"=>"Invalid Language ID");

	$params["language"] = $language_term->slug;
	}

	// Status 
	if( isset( $data["status_id"] ) && trim($data["status_id"]) != "" ){
		$status_term = get_term_by("term_id" ,$data["status_id"] ,"status");

		if($status_term == "") return array("error"=>true, "message"=>"Invalid Status ID");

		$params["status"] = $status_term->slug;
	}

	// Organizator 
	if( isset( $data["organizator_id"] ) && trim($data["organizator_id"]) != "" ){

		$organizator = get_post( $data["organizator_id"] );

		if($organizator == "" || $organizator->post_type != "organizator") return array("error"=>true, "message"=>"Invalid Organizator ID");

		$params["connected_type"] = "organizator_to_event";
		$params["connected_direction"] = "from";
		$params["connected_items"] = $data["organizator_id"];
	}


	// Attendant 
	if( isset( $data["attendant_id"] ) && trim($data["attendant_id"]) != "" ){
		$params["connected_type"] = "user_to_event";
		$params["connected_direction"] = "from";
		$params["connected_items"] = $data["attendant_id"];
	}


	// Gender
	if( isset( $data["gender"] ) && trim($data["gender"]) != "" ){
		$g = strtolower( $data["gender"] );
		$allowed_terms = array("f","m");

		if( !in_array($g, $allowed_terms) ) 
			return array("error"=>true, "message"=>"Invalid gender. It can be one of the following: " . implode(",",$allowed_terms) );

		$params["meta_query"][] = array(
			'key' => 'gjinia_dedikuar',
			'value' => $g,
			'compare' => '='
		);
	}

	// Periodic
	if( isset( $data["periodic"] ) && trim($data["periodic"]) != "" ){
		$p = strtolower( $data["periodic"] );
		$allowed_terms = array("jo","javore","mujore","vjetore");

		if( !in_array($p, $allowed_terms) )  
			return array("error"=>true, "message"=>"Invalid period. It can be one of the following: " . implode(",",$allowed_terms) );

		$params["meta_query"][] = array(
			'key' => 'periodike',
			'value' => $p,
			'compare' => '='
		);
	}
	
	// Start Date
	if( isset($data["start_date"]) && trim($data["start_date"]) != "" ){
		if( ($validDate=isValidDate($data["start_date"], "start date")) !== true )
			return array("error"=>true, "message"=>$validDate);


		$params["meta_query"][] = array(
			'key' => 'data_fillimit',
			'value' => getTimestamp( $data["start_date"] ),
			'compare' => '>=',
			'type' => "NUMERIC"
		);
	}

	// End Date
	if( isset($data["end_date"]) && trim($data["end_date"]) != "" ){

		if( ($validDate=isValidDate($data["end_date"], "end date")) !== true )
			return array("error"=>true, "message"=>$validDate);

		// CHECK AGAIN
		$params["meta_query"][] = array(
			'key' => 'data_fillimit',
			'value' => getTimestamp( $data["end_date"] ),
			'compare' => '<=',
			'type' => "NUMERIC"
		);
	}

	$p = get_query_var('page')?"page":"paged";
	$params["paged"] = (get_query_var($p)) ? get_query_var($p) : 1;
	$params["posts_per_page"] = 4;

	// $params = addPaginationParams( $params );
	$events_query = new WP_Query($params);
	return $events_query;
	//return $return_wp_query ?$events_query :getEvents( new WP_Query($params) );

}
function isValidDate( $date, $name="date" ){
	if(isValidTimeStamp( $date ) || strtotime($date)!=""){
		return true;
	}
	return "Invalid ".$name;
}
function getTimestamp($date){
	if( strtotime($date)!="" ){
		return strtotime($date);
    }
    return $date;
}
function isValidTimeStamp($timestamp){
    return ((string) (int) $timestamp == $timestamp) 
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
}






function get_time_left( $date ){
	$now = new DateTime();
	$event_date = new DateTime( $date );
	$interval = $event_date->diff($now);
	$time_left = "";
	if($interval->y == 0 && $interval->m == 0 && $interval->d == 0){
	  if($interval->h != 0){
	    $time_left .= $interval->h ." ". _n( "hour", "hours", $interval->h );
	  }
	  if($interval->i != 0){
	    $time_left .= " ".$interval->i ." ". _n( "minute", "minutes", $interval->i );
	  }
	}else{
	  if($interval->y != 0){
	    $time_left .= $interval->y ." ". _n( "year", "years", $interval->y );
	  }
	  if($interval->m != 0){
	    $time_left .= " ".$interval->m ." ". _n( "month", "months", $interval->m );
	  }
	  if($interval->d != 0){
	    $time_left .= " ".$interval->d ." ". _n( "day", "days", $interval->d );
	  }
	}

	$time_left .= $event_date->getTimestamp()>$now->getTimestamp() ?" left" :" ago";
	return trim($time_left);
}




function get_all_organizators(){
	global $_organizators;
	if($_organizators == "") $_organizators = new WP_Query(array("post_type"=>"organizator")); 
	return $_organizators;
}
function get_all_languages(){
	global $_languages;
	if($_languages == "") $_languages = get_terms("language", array("hide_empty"=>false) ); 
	return $_languages;
}
function get_all_statuses(){
	global $_statuses;
	if($_statuses == "") $_statuses = get_terms("status", array("hide_empty"=>false) );  
	return $_statuses;
}
function get_all_cities(){
	global $_cities;
	if($_cities == "") $_cities = get_terms("qyteti", array("hide_empty"=>false) );  
	return $_cities;
}
function get_all_categories(){
	global $_categories;
	if($_categories == "") $_categories = get_terms("events_category", array("hide_empty"=>false) );  
	return $_categories;
}
function get_all_periodics(){
	return array(
		array("value"=>"jo", "label"=>"Jo"),
		array("value"=>"javore", "label"=>"Javore"),
		array("value"=>"mujore", "label"=>"Mujore"),
		array("value"=>"vjetore", "label"=>"Vjetore")
	);
}
function get_all_genders(){
	return array(
		array("value"=>"f", "label"=>"Femer"),
		array("value"=>"m", "label"=>"Mashkull")
	);  
}






function check_recurring_events(){
	$events = new WP_Query( array(
		"post_type" => "event",
		"meta_query"=>array(
			"relation" => "AND",
			array(
				'key' => 'periodike',
				'value' => 'jo',
				'compare' => '!='
			),
			array(
				'key' => 'data_fillimit',
				'value' => getTimestamp( time() ),
				'compare' => '<=',
				'type' => "NUMERIC"
			)
		),
		"posts_per_page" => -1
	) );

	foreach($events->posts as $event){
		duplicate_event($event, true );
	}
	if(count($events->posts) > 0){
		check_recurring_events();
	}
}

function duplicate_event( $event , $dup=true ){


	if($dup):
		echo  $event->ID;
		
		$edit_post = array(
			'ID'           => $event->ID,
			'post_status' => 'past'
		);
		wp_update_post( $edit_post );
	endif;


	$meta = get_post_custom($event->ID);

	$my_post = array(
		'post_type' => 'event',
		'post_title'    => $event->post_title,
		'post_content'  => $event->post_content,
		'post_author'   => $event->post_author,
		'post_status'   => 'publish'	
	);
	// Insert the post into the database
	if($dup)
		$new_post_id = wp_insert_post( $my_post );


	$pid = $event->ID;
	if( isset($meta['parent_event']) ) $pid=$meta["parent_event"][0];
	update_post_meta($new_post_id, "parent_event", $pid );
	update_post_meta($event->ID, "parent_event", $pid );

	update_post_meta($event->ID, "duplicated", 'true');


	$df = date( 'm/d/y g:i A', intval( $meta['data_fillimit'][0] ) );
	$dm = date( 'm/d/y g:i A', intval( $meta['data_mbarrimit'][0] ) );
	$add = "";
	switch($meta['periodike'][0]){
		case "javore": $add=" 1 week"; break;
		case "mujore": $add=" 1 month"; break;
		case "vjetore": $add=" 1 year"; break;
	}

	update_field('field_52c1cadc2da7d', $meta['lokacioni'][0], $new_post_id );
	update_field('field_52c1caf32da7e', date( 'm/d/y g:i A', strtotime($df.$add) ) , $new_post_id );
	update_field('field_52c1cb052da7f', date( 'm/d/y g:i A', strtotime($dm.$add) ) , $new_post_id );
	update_field('field_52c1cb282da81', $meta['periodike'][0], $new_post_id );
	update_field('field_52c1ccc3fa3ab', $meta['gjinia_dedikuar'][0], $new_post_id );
	update_field('field_52c1cd8a3059f', $meta['koordinatat'][0], $new_post_id );

	set_post_thumbnail( $new_post_id, $meta["_thumbnail_id"][0] ); 

	/* Event Languages */
	$languages = wp_get_object_terms($event->ID, 'language');
	$language_ids = array();
	foreach($languages as $language){
		$language_ids[] = $language->term_id;
	}
	wp_set_object_terms( $new_post_id, $language_ids, 'language' );

	/* Event Statuses */
	$statuses = wp_get_object_terms($event->ID, 'status');
	$status_ids = array();
	foreach($statuses as $status){
		$status_ids[] = $status->term_id;
	}
	wp_set_object_terms( $new_post_id, $status_ids, 'status' );

	/* Event Category */
	$categories = wp_get_object_terms($event->ID, 'events_category');
	$category_ids = array();
	foreach($categories as $category){
		$category_ids[] = $category->term_id;
	}
	wp_set_object_terms( $new_post_id, $category_ids, 'events_category' );

	/* Event Cities */
	$cities = wp_get_object_terms($event->ID, 'qyteti');
	$city_ids = array();
	foreach($cities as $city){
		$city_ids[] = $city->term_id;
	}
	wp_set_object_terms( $new_post_id, $city_ids, 'qyteti' );


	$organizators = new WP_Query(array(
		"post_type" => "organizator",
		"connected_type" => "organizator_to_event",
		"connected_direction" => "to",
		"connected_items" => $event->ID
	));
	foreach($organizators->posts as $organizator){
		p2p_create_connection( 'organizator_to_event', array(
			'from' => $organizator->ID,
			'to' => $new_post_id
		) );
	}

	/*
	title
	description
	author
	image

	CUSTOM FIELDS:
	lokacioni - field_52c1cadc2da7d
	data_fillimit - field_52c1caf32da7e
	data_mbarrimit - field_52c1cb052da7f
	periodike - field_52c1cb282da81
	gjinia_dedikuar - field_52c1ccc3fa3ab
	koordinatat - field_52c1cd8a3059f

	TAX:
	language
	status
	events_category
	qyteti

	P2P:
	organizators
	*/
}


function parent_events(){
	$events = new WP_Query( array(
		"post_type" => "event",
		"meta_query"=>array(
			array(
				'key' => 'parent_event',
				'value' => $_REQUEST["id"],
				'compare' => '=',
				'type' => 'NUMERIC'
			)
		) ,
		"posts_per_page" => -1,
		"post_status" => array("publish","past")
	) );

	foreach($events->posts as $event){
		echo $event->ID . ": ". $event->post_title . ", Date:". get_field("data_fillimit",$event->ID) .": ". $event->post_status. "<br>";
	}
}



function is_search_page(){
	return isset($_REQUEST["search_term"]) || is_search();
}
function is_date_page(){
	return isset($_REQUEST["bydate"]);
}
function is_filter_page(){
	return isset($_REQUEST["filter"]);
}




function add_b_item( $label, $url="" ){
	global $bitems;
	
	$i = array("label"=>$label);
	if($url!="") $i["url"] = $url;

	$bitems[] = $i;
}

function breadcrumb(){
	global $bitems;
	$bitems = array();
	$qo = get_queried_object();

	// Add Home
	if( !is_search_page() && !is_date_page() )
		add_b_item( "Home", home_url() );

	// Archive/Category Items
	if(is_tax()){
		add_b_item( $qo->name, get_term_link($qo) );
	}else if(is_archive()){
		$labels = $qo->labels;
		add_b_item( $labels->name, get_post_type_archive_link( $qo->name ) );
	}

	// Search Items
	if(is_search_page()){
		add_b_item( "Search", home_url() );
		if( isset($_REQUEST["search_term"]) )
			add_b_item( $_REQUEST["search_term"] );
		else
			add_b_item( $_REQUEST["s"] );
	}

	// Date Items 
	if(is_date_page()){
		add_b_item( "Date", home_url() );
		add_b_item( ucfirst( str_replace("_", " ", $_REQUEST["bydate"] ) ) );
	}

	// Filter Items
	if(is_filter_page()){
		add_b_item( "Filter" );
	}

	// Single Event
	if(is_single()){
		$pt = get_post_type_object( $qo->post_type );
		$labels = $pt->labels;
		add_b_item( $labels->name, get_post_type_archive_link( $pt->name ) );
		add_b_item( $qo->post_title );
	}

	if(is_author()){
        $userdata = $qo->data;
		add_b_item( $userdata->display_name, get_author_posts_url( get_current_user_id() ) );
	}

	echo generate_breadcrumb_html( $bitems );
}

function generate_breadcrumb_html( $items ){
	$html = "";
	$total = count($items);
	$c = 0;
	foreach($items as $item){
		$cls = array("breadcrumb_item");

		if($item["label"] == "") continue;

		if($c != $total && $c != 0){
			$html .= "<li class='breadcrumb_arrow icon-chevron-right'></li>";
		}

		if($c == 0) $cls[] = "first";
		if($c == $total-1) $cls[] = "last";

		$html .= "<li class='".implode(" ", $cls)."'>";
			if( isset($item["url"]) ) $html .= "<a href='".$item["url"]."'>";
				$html .= ucfirst( $item["label"] );
			if( isset($item["url"]) ) $html .= "</a>";
		$html .= "</li>";

		$c++;
	}
	return $html;
}

/*

<option value="category_id" selected>Category</option>
<option value="city_id">City</option>
<option value="language_id">Language</option>
<option value="status_id">Status</option>
<option value="organizator_id">Organizator</option>
<option value="periodic">Periodic</option>
<option value="gender">Gender</option>
<option value="start_date">Start Date</option>
<option value="end_date">End Date</option>

*/

function get_filter_data( $filter_key , $filter_value ){
	$data;
	switch($filter_key){
		case "category_id":
			$c = get_term_by("id", $filter_value, "events_category" );
			$data = array("label"=>"Category", "value" => $c->name);
			break;
		case "city_id":
			$c = get_term_by("id", $filter_value, "qyteti" );
			$data = array("label"=>"City", "value" => $c->name);
			break;
		case "language_id":
			$c = get_term_by("id", $filter_value, "language" );
			$data = array("label"=>"Language", "value" => $c->name);
			break;
		case "status_id":
			$c = get_term_by("id", $filter_value, "status" );
			$data = array("label"=>"Status", "value" => $c->name);
			break;
		case "organizator_id":
			$c = get_post($filter_value);
			$data = array("label"=>"Organizator", "value" => $c->post_title);
			break;
		case "periodic":
			$data = array("label"=>"Periodic", "value" => ucfirst($filter_value) );
			break;
		case "gender":
			$data = array("label"=>"Gender", "value" => $filter_value=="f"?"Femer":"Mashkull" );
			break;
		case "start_date":
			$data = array("label"=>"Start Date", "value" => $filter_value);
			break;
		case "end_date":
			$data = array("label"=>"End Date", "value" => $filter_value);
			break;
	}
	return $data;
}



function reset_events_col(){
	global $event_item_col;
	$event_item_col = 6;
}
function set_events_col( $col ){
	global $event_item_col;
	$event_item_col = $col;
}
reset_events_col();










function notify_users(){
	// Eventet 24 ore perpara
	$now = strtotime("now");
	$after24hours = strtotime("now + 1 day");
	echo date( 'm/d/y g:i A', $now );
	echo "<br>";
	echo date( 'm/d/y g:i A', $after24hours );
	echo "<br>";

	$events = searchEvents(array(
		"start_date" => $now,
		"end_date" => $after24hours
	));

	$users_to_notify = array();

	foreach($events->posts as $event){
		echo $event->post_title."<br>";

		$now = new DateTime();
		$event_date = new DateTime( get_field("data_fillimit", $event->ID) );
		$interval = $event_date->diff($now);

		if( $interval->h <= 3 ){
			notify_user_for_event( 3, $event, $interval, $users_to_notify );
		}else if($interval->h <= 24){
			notify_user_for_event( 24, $event, $interval, $users_to_notify );
		}

		print_r($interval);
		echo "<br>";

        print_r($users);
		echo "<br>";
	}
	echo "<br>";

	foreach($users_to_notify as $user_id=>$events){

		if( get_user_meta($user_id, 'has_app', true) ){
			// Notify by Push Notification
			notify_by_push( $user_id, $events );
		}else if( get_user_meta($user_id, "phone_nr", true ) != "" ){
			// Notify by SMS
			notify_by_sms( $user_id, $events );
		}

		notify_by_email( $user_id, $events );

	}
}

function notify_user_for_event( $time_left, $event, $interval,  &$users_to_notify ){

	$users = get_users( array(
      'connected_type' => 'user_to_event',
      'connected_items' => $event,
      'connected_meta' => array( 'notified_'.$time_left => "0" )
    ) );

	foreach($users as $user){
		$user_data = $user->data;
		if( !isset($users_to_notify[$user->ID]) ) $users_to_notify[$user->ID] = array();
		$users_to_notify[$user->ID][] = array(
			"time_left" => $time_left,
			"event" => $event,
			"connection_id" => $user->p2p_id,
			"interval" => $interval
		);
	}

}


function notify_by_push( $user_id, $events ){
	set_notified_for_user($events);
}
function notify_by_sms( $user_id, $events ){
	set_notified_for_user($events);

	$phone_number = get_user_meta($user_id,"phone_nr",true);

	$msg = "Reminder: ";
	$items = array();
	foreach($events as $ev){
		$event = $ev["event"];
		$time_left = $ev["interval"];
		$_msg = $event->post_title . " in ";
		if($time_left->h > 0) $_msg.= $time_left->h."h ";
		if($time_left->i > 0) $_msg.= $time_left->i."m";
		$items[] = $_msg;
	}
	$msg .= implode(", ",$items);

	// this line loads the library 
	require('twilio/Twilio.php'); 
	 
	$account_sid = 'AC5e8686c1ec78ee9524aa84da95b9cdf6'; 
	$auth_token = 'eac453356757dedf500ed9d06d138148'; 
	$client = new Services_Twilio($account_sid, $auth_token); 
	 
	$r = $client->account->messages->create(array( 
		'To' => $phone_number,
		'From' => "+13852101334", 
		'Body' => $msg,   
	));
}
function notify_by_email( $user_id, $events ){
	set_notified_for_user($events);

	$user_info = get_userdata($user_id);
	$user_info = $user_info->data;

	$msg = "Reminder: ";
	$items = array();
	foreach($events as $ev){
		$event = $ev["event"];
		$time_left = $ev["interval"];
		$_msg = $event->post_title . " in ";
		if($time_left->h > 0) $_msg.= $time_left->h."h ";
		if($time_left->i > 0) $_msg.= $time_left->i."m";
		$items[] = $_msg;
	}
	$msg .= implode(", ",$items);

	add_filter( 'wp_mail_content_type', 'set_html_content_type' );

	wp_mail($user_info->user_email, "Events Reminder", $msg );

	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	
}

function set_html_content_type() {

	return 'text/html';
}

function set_notified_for_user( $events ){
	foreach($events as $event){
		p2p_update_meta( $event["connection_id"], "notified_".$event["time_left"], "1" );
	}
}
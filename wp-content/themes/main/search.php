<?php get_header(); ?>

    <?php  
      get_template_part("part","breadcrumb");
    ?>
    
    <div class="container">

      <section class="col-lg-3 sidebar">
          <?php get_sidebar(); ?>
      </section><!-- End of sidebar -->

      <section class="thumbnails col-lg-9 np-l" role="main">

        <?php 
        global $events_data, $_fields;

        $params = $_GET;
        $events_data = array();
        foreach($params as $key=>$value)
          if(in_array($key, $_fields))
            $events_data[$key] = $value;

        $events = searchEvents($events_data);
        if( is_array($events) && isset( $events["error"] ) ):

          // If there is a error, show it
          ?>

            <p class="bg-danger"><?php echo $events["message"]; ?></p>

          <?php 
        else:

            if($events->have_posts()):

              while($events->have_posts()): $events->the_post(); 
                get_template_part("event","item"); 
              endwhile; 

            else: 
              ?>

              <div class="col-lg-12">
                  <div class="no_results">No Events found that match the search</div>
              </div>

              <?php 
            endif; 

        endif;
        ?>


        <?php
        global $wp_query;
        $wp_query = $events;
        if(function_exists("wp_pagenavi"))
          wp_pagenavi();
        ?>

      </section><!-- End of thumbanil -->

<?php
  get_footer(); 
?>
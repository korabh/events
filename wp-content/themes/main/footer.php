	<?php wp_footer(); ?>
	</div>
	<footer>
		<div class="container">
			<div class="col-lg-3 np-l">
			<ul class="footer-nav">
        <li class="title">Sitemap</li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Support</a></li>
        <li><a href="#">Private Policy</a></li>
      </ul>
			</div>
			<div class="col-lg-6 np-l">
			<ul class="footer-nav">
        <li class="title">Developers</li>
        <li><a href="#">API</a></li>
        <li><a href="#">Terms of Use</a></li>
        <li><a href="#">Contect Us</a></li>
        <li><a href="#">Press</a></li>
        <li><a href="#">Instructions</a></li>
      </ul>
			</div>
			<div class="col-lg-2 get-top">
				<a href="#"><i class="icon-arrow-up icon-2x"></i></a>
			</div>
		</div>
	</footer>
	<div id="copyright">
		<p>Copyright © Muslim Meeting 2014. All Rights Reserved</p>
	</div>
</body>
</html>
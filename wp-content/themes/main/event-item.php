<?php 
  $time_left = get_time_left( get_field("data_fillimit") );

  $now = new DateTime();
  $event_date = new DateTime( get_field("data_fillimit") );
  $past = $event_date->getTimestamp()<$now->getTimestamp();

  $event_city = wp_get_post_terms( get_the_ID(), "qyteti" );
  $has_city = false;
  if(count($event_city) > 0){
    $event_city = $event_city[0];
    $has_city = true;
  }
  
  $post_cats = wp_get_post_terms( get_the_ID(), "events_category" );

  global $event_item_col;
?>

<article class="col-lg-<?php echo $event_item_col; ?> item <?php echo $past?"past_event":""; ?>">
  <?php if($past): ?>
    <div class="image_container">
      <span class="ended_tag">ENDED</span>
    </div>
  <?php endif; ?> 
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', array('class' => 'img-rounded')); ?></a>
  <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
  <h3><a href="<?php the_permalink(); ?>"><?php the_field("lokacioni") ?></a></h3>
  <ul class="details">
    <li><?php echo $time_left; ?></li>
		<!-- <li><i class="fa fa-map-marker fa-2x"></i></li> -->
    <?php if($has_city): ?>
		  <li><i class="icon-map-marker icon-1x"></i></li>
      <li><a href="<?php echo get_term_link($event_city); ?>"><?php echo $event_city->name; ?></a></li>
    <?php endif; ?>
  </ul>
</article>
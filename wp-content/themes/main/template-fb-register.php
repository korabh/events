<?php  
	/* Template Name: Register */
?>

<?php  
	if(is_user_logged_in()){
		wp_redirect(home_url());
		exit;
	}
?>

<?php get_header(); ?>


    <div class="container">
	<h2>Facebook Registration</h2>
	<div class="fb_register">
		<form id="loginform" method="post" action="http://localhost/events/wp-login.php">
			
			<?php if(isset($_REQUEST["username"]) && username_exists($_REQUEST["username"])): ?>
			<div class="error_msg">
				Username <?php echo $_REQUEST["username"] ?> exists. Please choose another username.
			</div>
			<?php endif; ?>

			<input type="hidden" id="fb_register_submit" name="fb_register_submit" value="1">
			<input type="text" placeholder="Username" id="username" name="username" value="<?php echo isset($_REQUEST["username"])?$_REQUEST["username"]:""; ?>">
			<input type="text" placeholder="Phone Number" id="phone_nr" name="phone_nr" value="<?php echo isset($_REQUEST["phone_nr"])?$_REQUEST["phone_nr"]:""; ?>">
			<input type="password" placeholder="Password" id="password" name="password" value="<?php echo isset($_REQUEST["password"])?$_REQUEST["password"]:""; ?>">

			<input type="hidden" id="redirect_to" name="redirect_to" value="<?php echo $_REQUEST["redirect_to"] ?>">
			<input type="hidden" id="action" name="action" value="social_connect">
			<input type="hidden" id="social_connect_provider" name="social_connect_provider" value="<?php echo $_REQUEST["social_connect_provider"] ?>">
			<input type="hidden" id="social_connect_signature" name="social_connect_signature" value="<?php echo $_REQUEST["social_connect_signature"] ?>">
			<input type="hidden" id="social_connect_access_token" name="social_connect_access_token" value="<?php echo $_REQUEST["social_connect_access_token"] ?>">
			<button type="submit">Register</button>
		</form>
	</div>


<?php
get_footer(); 
?>


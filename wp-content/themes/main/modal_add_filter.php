<?php 
  $organizators = get_all_organizators();
  $languages = get_all_languages();
  $statuses = get_all_statuses();
  $cities = get_all_cities();
  $categories = get_all_categories();
  $periodics = get_all_periodics();
  $genders = get_all_genders();
?>
<div class="modal fade" id="addFilter" tabindex="-1" role="dialog" aria-labelledby="addFilterLabel" aria-hidden="true">
  <form action="<?php echo HOME; ?>">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="addFilterLabel">Add Filter</h4>
        </div>
        <div class="modal-body">
        <div id="filters">
            <select name="filter_key" class="form-control" id="filter_key">
              <option value="category_id" selected>Category</option>
              <option value="city_id">City</option>
              <option value="language_id">Language</option>
              <option value="status_id">Status</option>
              <option value="organizator_id">Organizator</option>
              <option value="periodic">Periodic</option>
              <option value="gender">Gender</option>
              <option value="start_date">Start Date</option>
              <option value="end_date">End Date</option>
            </select>
						<br>
            <!-- Category -->
            <select name="category_id" id="category_id" class="form-control filter_field">
              <?php foreach($categories as $category): ?>
                <option value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- City -->
            <select name="city_id" id="city_id" class="form-control filter_field">
              <?php foreach($cities as $city): ?>
                <option value="<?php echo $city->term_id; ?>"><?php echo $city->name; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Language -->
            <select name="language_id" id="language_id" class="form-control filter_field">
              <?php foreach($languages as $language): ?>
                <option value="<?php echo $language->term_id; ?>"><?php echo $language->name; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Status -->
            <select name="status_id" id="status_id" class="form-control filter_field">
              <?php foreach($statuses as $status): ?>
                <option value="<?php echo $status->term_id; ?>"><?php echo $status->name; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Organizator -->
            <select name="organizator_id" id="organizator_id" class="form-control filter_field">
              <?php foreach($organizators->posts as $organizator): ?>
                <option value="<?php echo $organizator->ID; ?>"><?php echo $organizator->post_title; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Periodic -->
            <select name="periodic" id="periodic" class="form-control filter_field">
              <?php foreach($periodics as $periodic): ?>
                <option value="<?php echo $periodic['value']; ?>"><?php echo $periodic['label']; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Gender -->
            <select name="gender" id="gender" class="form-control filter_field">
              <?php foreach($genders as $gender): ?>
                <option value="<?php echo $gender['value']; ?>"><?php echo $gender['label']; ?></option>
              <?php endforeach; ?>
            </select>

            <!-- Start date -->
            <input type="text" name="start_date" id="start_date" class="form-control filter_field datepicker">

            <!-- End date -->
            <input type="text" name="end_date" id="end_date" class="form-control filter_field datepicker">

        </div>
				
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" value="Add">Add filter</button>
        </div>
      </div>
    </div>
  </form>
</div>
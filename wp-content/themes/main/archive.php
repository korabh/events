<?php get_header(); ?>

    <?php  
      get_template_part("part","breadcrumb");
    ?>

    <div class="container">
      <section id="category_title">

      </section>
    
      
      <section class="col-lg-3 sidebar">
          <?php get_sidebar(); ?>
      </section><!-- End of sidebar -->

      <section class="thumbnails col-lg-9 np-l" role="main">
        
        <?php  

          $qo = get_queried_object();
          
          $search_by = "";
          switch( $qo->taxonomy ){
            case "qyteti": $search_by="city_id"; break;
            case "events_category": $search_by="category_id"; break;
            case "status": $search_by="status_id"; break;
            case "language": $search_by="language_id"; break;
          }

          $data = array($search_by => $qo->term_id);
          $events = searchEvents($data);
        ?>
        <h2 class="thumbs_title">
          <?php 
            echo $qo->name; 
          ?>
        </h2>

        <?php
        if($events->have_posts()):

          while($events->have_posts()): $events->the_post(); 
            get_template_part("event","item"); 
          endwhile; 

        else: 
          ?>

          <div class="col-lg-12">
              <div class="no_results">No Events found that match the search</div>
          </div>

          <?php 
        endif; 
        ?>


        <?php
        if(function_exists("wp_pagenavi"))
          wp_pagenavi();
        ?>

      </section><!-- End of thumbanil -->

<?php
get_footer(); 
?>
<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'events');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't{Cw@/@Lg^GVXxHd4+_~peQ{}>}yOk.uK(E_H)c+9pdziN#S]~98=o<L[9v~ZVk-');
define('SECURE_AUTH_KEY',  'p6&oIAELmd@(}56!8YN`|}678W C<gi$mL `APm1KNUQDqc!nY:?VA2aeU[$--Xy');
define('LOGGED_IN_KEY',    'pzcAYJJ 1kj_0j9ZG)?f^^((c)$ eom}[x23DB8FB6z<cDcH:FH FhkMLOy/Rxma');
define('NONCE_KEY',        'X9>_1 f)$]PR-duwV4R|alniv-M]qzdK$SQg/g!uUy L|TOcR[PCQX$(DPUt-><N');
define('AUTH_SALT',        'L5*]-KTvir%#>|q=`kN@=7DZ]:/CJ&x{mRVE)} ^wr%#O: P4fnu:p+EO-j7nclF');
define('SECURE_AUTH_SALT', '1-cI(;+3DO<Ch)[=Ti`@0#>DxGaZ!cpWZq8)L8.X.b1W|V[HL`zdDZD+PdB+s{*n');
define('LOGGED_IN_SALT',   '5zU>>B8Y}rAh!E`/lSi39|?o;!vg|E-;i~vT|F7E?f!<1#zx|=`!sFO%k2]ljcne');
define('NONCE_SALT',       '*a$PtqcrR0J3jkF~dN#-8Ro.PQ-1B@U3%X7z0iRqti|tl->J*[?NOY vO^,Fn-pW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
